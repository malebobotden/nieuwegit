﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlHelp : MonoBehaviour
{

    public GameObject scrollUI;

    // When player enters scroll collider open UI
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
           
            scrollUI.SetActive(true);
            
        }
    }

    // When player leaves scroll collider close UI
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            scrollUI.SetActive(false);

        }
    }



}
