﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public GameObject pickupEffect;
    public float multiplier = 1.4f;
    public float duration = 250f;

    // Player collides with powerup
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
           StartCoroutine (Pickup(other));
        }
    }

    IEnumerator Pickup(Collider Player)
    {
        //Spawn effect
        Instantiate(pickupEffect, transform.position, transform.rotation);

        //Apply effect to the player
        Physics.gravity = new Vector3(0, -0.0F, 0);

        //disable graphics powerup
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Collider>(). enabled = false;

        // Wait x amount of seconds
        yield return new WaitForSeconds(duration);

        //Reverse the effect on our player
        Physics.gravity = new Vector3(0, -10, 0);

        //Remove power up object
        Destroy(gameObject);
    }
}
