﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    bool gameHasEnded = false;

    public float restartDelay = 1f;
    public GameObject completeLevelUI;
    public GameObject completePartUI;

    // after first dungeon room next level text will appear
    public void CompletePart()
    {
        completePartUI.SetActive(true);
    }

    // the end of the game
    public void CompleteLevel()
    {
        completeLevelUI.SetActive(true);
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
    }


    // level restarting
    public void EndGame()
    {
        if(gameHasEnded == false)
        {
            gameHasEnded = true;
            Physics.gravity = new Vector3(0, -10, 0);
            Restart();
            Invoke("Restart", restartDelay);
        }
    }

    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
