﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{

    public GameObject door;

    bool isOpened = false;

    //When player picks up key, both the key and the door will be destroyed
    private void OnTriggerEnter(Collider other)
    {
        if (!isOpened)
        {
            
            Destroy(gameObject);
            Destroy(door); 
        }

    }
}

