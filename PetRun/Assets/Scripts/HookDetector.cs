﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script checks if the colliding object has the hookable tag
public class HookDetector : MonoBehaviour
{

	public GameObject player;

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Hookable")
		{
			player.GetComponent<GrapplingHook>().hooked = true;
			player.GetComponent<GrapplingHook>().hookedObj = other.gameObject;
		}
	}


}
