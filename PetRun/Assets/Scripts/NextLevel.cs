﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevel : MonoBehaviour
{
    public GameManager gameManager;
    public Transform checkpoint;
    
   
    private void OnTriggerEnter(Collider other)
    {
        if (gameObject.tag == "Player")
        {
            gameManager.CompletePart();
         
        }
    }
}
