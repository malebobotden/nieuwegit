﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 4f;

    private Rigidbody rigidBody;
    public bool playerIsOnTheGround;
    public bool canDoubleJump;

    // Update is called at start of the game
    private void Awake()
    {
        rigidBody = GetComponentInChildren<Rigidbody>();

        //jumping
        playerIsOnTheGround = true;
        canDoubleJump = true;
    }

    // Update is called once per frame
    private void Update()
    {
        //walking
        float vert = Input.GetAxis("Vertical");
        float hor = Input.GetAxis("Horizontal");

        Vector3 moveDir = vert * transform.forward + hor * transform.right;

        rigidBody.MovePosition(transform.position + moveDir.normalized * Time.deltaTime * moveSpeed);

        //jumping first time
        if (Input.GetButtonDown("Jump") && !playerIsOnTheGround && canDoubleJump)
        {
            rigidBody.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
            playerIsOnTheGround = false;
            canDoubleJump = false;
        }
        //jumping second time
        else if(Input.GetButtonDown("Jump") && playerIsOnTheGround)
        {
            rigidBody.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
            playerIsOnTheGround = false;
            canDoubleJump = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Player is safe to walk
        if (collision.gameObject.tag == "Floor")
        {
            playerIsOnTheGround = true;
        }

        //Player has fallen down
        if (collision.gameObject.tag == "Death")
        {
          FindObjectOfType<GameManager>().EndGame();
        }
    }

}
